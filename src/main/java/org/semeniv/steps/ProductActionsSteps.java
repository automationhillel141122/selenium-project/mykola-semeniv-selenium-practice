package org.semeniv.steps;

import org.semeniv.pages.BasePage;
import org.semeniv.pages.products.ProductsPage;

public class ProductActionsSteps {
    public void addProductToCart(String category, String subcategory, String productName){
        ProductsPage productsPage;
        if (subcategory == null){
            new BasePage().categoryMenu().selectMainCategory(category);
        } else {
            new BasePage().categoryMenu().selectSubCategory(category, subcategory);
        }

        productsPage = new ProductsPage();

        productsPage.addProductToCart(productName);
    }
}

