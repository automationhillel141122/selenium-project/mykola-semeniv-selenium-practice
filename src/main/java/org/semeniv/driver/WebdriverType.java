package org.semeniv.driver;

public enum WebdriverType {
    CHROME,
    FIREFOX,
    SAFARI
}

