package org.semeniv.utils;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.IOException;
import java.util.Date;

public class MyFileUtils {

    public static void main(String[] args) {
        File file = generateTestFile();
        System.out.println();
    }

    public static File generateTestFile() {
        File template = new File("files/text.txt");
        String baseName = FilenameUtils.getBaseName(template.getName());
        String extension = FilenameUtils.getExtension(template.getName());
        File fileTemp = new File("files/"
                + baseName
                + new Date().getTime()
                + "."
                + extension);
        try {
            FileUtils.copyFile(template, fileTemp);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        fileTemp.deleteOnExit();
        return fileTemp;
    }

    public static void createDownloadDirectory() {
        createDirectory(DirectoryFor.DOWNLOAD);
    }

    public static void createScreenshotsDirectory(){
        createDirectory(DirectoryFor.SCREENSHOTS);
    }

    private static void createDirectory(DirectoryFor directoryFor) {
        File directory = new File(directoryFor.getDirName());
        if (directory.exists()) {
            try {
                FileUtils.cleanDirectory(directory);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } else {
            directory.mkdir();
        }
    }

    public static File waitTillFileDownloaded(String fileName) throws Exception {
        File downloadDir = new File(DirectoryFor.DOWNLOAD.getDirName());
        File file = new File(downloadDir, fileName);
        int count = 0;
        //TODO fix this after bug will be fixed
        while (count < 5) {
            if (file.exists()) {
                count = 0;
                break;
            }
            pause(1000);
            count++;
        }
        if (count > 0) {
            throw new Exception("File is not exist!!!");
        }

        while (count < 60) {
            long fileSizeBefore = file.length();
            pause(1000);
            long fileSizeAfter = file.length();
            if (fileSizeBefore == fileSizeAfter){
                return file;
            }
            count++;
        }
        throw new Exception("File is too big!!!");
    }

    public enum DirectoryFor {
        DOWNLOAD("download"),
        SCREENSHOTS("screenshots");

        private String dirName;

        DirectoryFor(String dirName) {
            this.dirName = dirName;
        }

        public String getDirName() {
            return dirName;
        }
    }

    public static void pause(long msec) {
        try {
            Thread.sleep(msec);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
