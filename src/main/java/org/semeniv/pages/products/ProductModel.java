package org.semeniv.pages.products;

public class ProductModel {
    private String productName;
    private Long price;

    public ProductModel(String productName, Long price) {
        this.productName = productName;
        this.price = price;
    }

    public String productName() {
        return productName;
    }

    public ProductModel setProductName(String productName) {
        this.productName = productName;
        return this;
    }

    public Long price() {
        return price;
    }

    public ProductModel setPrice(Long price) {
        this.price = price;
        return this;
    }

    @Override
    public String toString() {
        return "ProductModel{" +
                "productName='" + productName + '\'' +
                ", price=" + price +
                '}';
    }
}