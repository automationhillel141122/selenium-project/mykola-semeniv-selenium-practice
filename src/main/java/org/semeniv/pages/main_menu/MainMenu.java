package org.semeniv.pages.main_menu;

import org.openqa.selenium.WebElement;
import org.semeniv.driver.WebdriverHolder;
import org.openqa.selenium.By;
import org.semeniv.pages.LoginPage;

public class MainMenu {

    public LoginPage selectLogin(){
        selectMenuItem(MainMenuItem.LOGIN);
        return new LoginPage();
    }

    public void selectRegister(){
        selectMenuItem(MainMenuItem.REGISTER);
    }

    public void selectWishList(){
        selectMenuItem(MainMenuItem.WISH_LIST);
    }
    public void selectShoppingCart(){
        selectMenuItem(MainMenuItem.SHOPPING_CART);
    }

    public void selectMyAccount(){
        selectMenuItem(MainMenuItem.ACCOUNT);
    }

    private void selectMenuItem(MainMenuItem menuItem){
        getMenuItemWebElement(menuItem).click();
    }

    public boolean isMenuItemVisible(MainMenuItem menuItem){
        return getMenuItemWebElement(menuItem).isDisplayed();
    }

    private WebElement getMenuItemWebElement(MainMenuItem menuItem){
        return WebdriverHolder.getInstance().getDriver()
                .findElement(By.cssSelector("a.ico-" + menuItem.value()));
    }
    public LoginPage selectLogout(){
        selectMenuItem(MainMenuItem.LOGOUT);
        return new LoginPage();
    }


}
