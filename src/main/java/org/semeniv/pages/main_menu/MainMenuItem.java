package org.semeniv.pages.main_menu;

public enum MainMenuItem {
    LOGIN("login"),
    LOGOUT("logout"),
    ACCOUNT("account"),
    REGISTER("register"),
    WISH_LIST("wishlist"),
    SHOPPING_CART("cart");

    private String value;

    MainMenuItem(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }


}