package org.semeniv.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.semeniv.driver.WebdriverHolder;
import org.semeniv.pages.category_menu.CategoryMenu;
import org.semeniv.pages.main_menu.MainMenu;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.semeniv.pages.main_menu.MainMenuItem;
import org.semeniv.pages.products.ProductsPage;

import java.time.Duration;

public class BasePage {
    String itemBox = "//div[@class='item-box']//div[@class='details']//a[text()='%s']";

    public BasePage() {
        PageFactory.initElements(WebdriverHolder.getInstance().getDriver(), this);
    }

    public MainMenu mainMenu(){
        return new MainMenu();
    }

    public CategoryMenu categoryMenu(){
        return new CategoryMenu();
    }

    public BasePage selectCurrency(AppCurrency currency){
        WebElement customerCurrency = WebdriverHolder.getInstance().getDriver()
                .findElement(By.id("customerCurrency"));
        new Select(customerCurrency)
                .selectByVisibleText(currency.currencyText());
        return new BasePage();
    }

    public BasePage search(String searchPattern){
        WebElement searchForm = WebdriverHolder.getInstance().getDriver().findElement(By.id("small-search-box-form"));
        searchForm.findElement(By.xpath(".//input")).sendKeys(searchPattern);
        searchForm.findElement(By.xpath(".//button")).click();
        return new BasePage();
    }
    public ProductsPage addProductToCart(String productName) {
        WebDriver driver = WebdriverHolder.getInstance().getDriver();
        WebElement element = driver.findElement(By.xpath(itemBox.formatted(productName)));
        WebElement productItem = element.findElement(By.xpath("./../.."));
        WebDriverWait wait = new WebDriverWait(WebdriverHolder.getInstance().getDriver(), Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='bar-notification']")));
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@id='bar-notification']")));
        productItem.findElement(By.cssSelector(".buttons .product-box-add-to-cart-button")).click();
        return new ProductsPage();
    }

    public ProductsPage addToWishList(String productName) {
        WebDriver driver = WebdriverHolder.getInstance().getDriver();
        WebElement element = driver.findElement(By.xpath(itemBox.formatted(productName)));
        WebElement productItem = element.findElement(By.xpath("./../.."));
        WebDriverWait wait = new WebDriverWait(WebdriverHolder.getInstance().getDriver(), Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='bar-notification']")));
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@id='bar-notification']")));
        productItem.findElement(By.cssSelector(".buttons .product-box-add-to-wishlist-button")).click();
        return new ProductsPage();
    }

    public boolean ifUserIsLoggedIn(){
        return mainMenu().isMenuItemVisible(MainMenuItem.ACCOUNT)
                && mainMenu().isMenuItemVisible(MainMenuItem.LOGOUT);
    }

    public boolean ifUserIsLoggedOut(){
        return mainMenu().isMenuItemVisible(MainMenuItem.REGISTER)
                && mainMenu().isMenuItemVisible(MainMenuItem.LOGIN);
    }

}