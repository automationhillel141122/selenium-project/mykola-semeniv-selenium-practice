package org.semeniv.stage2;

import org.openqa.selenium.By;
import org.semeniv.additional.BaseTest;
import org.semeniv.driver.WebdriverHolder;
import org.semeniv.pages.BasePage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class AddItemToCart extends BaseTest {

    @Test
    public void addItem() {

        String count1 = WebdriverHolder.getInstance().getDriver().findElement(By.xpath("//span[@class=\"cart-qty\"]")).getText();
        String beforeAddCount = count1.replace("(", "").replace(")", "");
        int beforeCountInt = Integer.parseInt(beforeAddCount);

        new BasePage()
                .categoryMenu()
                .selectSubCategory("Computers", "Notebooks")
                .addProductToCart("Samsung Series 9 NP900X4C Premium Ultrabook");

        String count2 = WebdriverHolder.getInstance().getDriver().findElement(By.xpath("//span[@class=\"cart-qty\"]")).getText();
        String afterAddCount = count2.replace("(", "").replace(")", "");
        int afterCountInt = Integer.parseInt(afterAddCount);

        Assert.assertEquals(beforeCountInt+1, afterCountInt);
    }
}