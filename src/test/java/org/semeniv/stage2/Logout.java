package org.semeniv.stage2;

import org.semeniv.additional.BaseTest;
import org.semeniv.pages.LoginPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Logout extends BaseTest {

    @Test
    public void logout() {

        boolean isLoggedOut = new LoginPage()
                .mainMenu()
                .selectLogout()
                .ifUserIsLoggedOut();
        Assert.assertTrue(isLoggedOut);
    }
}
