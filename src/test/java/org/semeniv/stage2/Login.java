package org.semeniv.stage2;

import org.semeniv.additional.BaseTest;
import org.semeniv.pages.LoginPage;
import org.semeniv.utils.PropertyReader;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Login extends BaseTest {

    @Test
    public void login() {
        clearCash();

        boolean isLoggedIn = new LoginPage()
                .mainMenu()
                .selectLogin()
                .login(PropertyReader.getProperty("user.name"), PropertyReader.getProperty("user.password"))
                .ifUserIsLoggedIn();
        Assert.assertTrue(isLoggedIn);
    }
}