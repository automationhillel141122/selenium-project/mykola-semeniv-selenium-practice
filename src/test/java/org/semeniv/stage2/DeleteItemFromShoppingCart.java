package org.semeniv.stage2;

import org.openqa.selenium.By;
import org.semeniv.additional.BaseTest;
import org.semeniv.driver.WebdriverHolder;
import org.semeniv.pages.BasePage;
import org.semeniv.pages.products.ProductsPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DeleteItemFromShoppingCart extends BaseTest {

    @Test
    public void deleteItem() {

        ProductsPage productsPage = new BasePage()
                .categoryMenu()
                .selectSubCategory("Computers", "Notebooks")
                .addProductToCart("Samsung Series 9 NP900X4C Premium Ultrabook")
                .addProductToCart("Asus N551JK-XO076H Laptop")
                .addProductToCart("Lenovo Thinkpad X1 Carbon Laptop");

        productsPage
                .mainMenu()
                .selectShoppingCart();

        String summaBeforeRemove = WebdriverHolder.getInstance().getDriver().findElement(By.xpath("//span[@class=\"value-summary\"]")).getText();

        Assert.assertEquals(summaBeforeRemove, "$4,450.00");

        WebdriverHolder.getInstance().getDriver().findElement(By.xpath("//button[@class=\"remove-btn\"]")).click();

        String summaAfterRemove = WebdriverHolder.getInstance().getDriver().findElement(By.xpath("//span[@class=\"value-summary\"]")).getText();

        Assert.assertEquals(summaAfterRemove, "$2,860.00");
    }
}
