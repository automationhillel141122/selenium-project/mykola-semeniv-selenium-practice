package org.semeniv.listeners;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.service.ExtentTestManager;
import com.aventstack.extentreports.testng.listener.ExtentITestListenerClassAdapter;
import io.qameta.allure.Attachment;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.semeniv.driver.WebdriverHolder;
import org.semeniv.utils.MyFileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestContext;
import org.testng.ITestResult;

import java.io.File;
import java.io.IOException;
import java.util.Date;

@Slf4j
public class MyTestListener extends ExtentITestListenerClassAdapter {

    @Override
    public void onTestStart(ITestResult result) {
        log.info("Test: " + result.getName() + " started!");
        super.onTestStart(result);
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        log.info("Test: " + result.getName() + " finished successfully!");
        super.onTestSuccess(result);
    }

    @Override
    public void onTestFailure(ITestResult result) {
        log.error("Test: " + result.getName() + " finished failed!");
        byte[] bytes = makeScreenshot(result);
        ExtentTest test = ExtentTestManager.getTest(result);
        String decodedString = new String(new Base64().encode(bytes));
        test.addScreenCaptureFromBase64String(decodedString, "image");
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        log.info("Test: " + result.getName() + " finished skipped!");
        super.onTestSkipped(result);
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        super.onTestFailedButWithinSuccessPercentage(result);
    }

    @Override
    public void onTestFailedWithTimeout(ITestResult result) {
        super.onTestFailedWithTimeout(result);
    }

    @Override
    public void onStart(ITestContext context) {
        super.onStart(context);
    }

    @Override
    public void onFinish(ITestContext context) {
        super.onFinish(context);
    }

    @Attachment(value = "Page screenshot", type = "image/png")
    private byte[] makeScreenshot(ITestResult testResult){
        log.info("Making screenshot...");
        File screenshotAs = ((TakesScreenshot) WebdriverHolder.getInstance().getDriver())
                .getScreenshotAs(OutputType.FILE);
        File screenshot = new File(MyFileUtils.DirectoryFor.SCREENSHOTS.getDirName(),
                testResult.getName() + new Date().getTime() + ".png");

        try {
            FileUtils.copyFile(screenshotAs, screenshot);
            log.info("Screenshot saved.\nScreenshot path: " + screenshot.getAbsolutePath());
            return FileUtils.readFileToByteArray(screenshot);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}