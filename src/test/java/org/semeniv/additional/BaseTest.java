package org.semeniv.additional;

import org.semeniv.driver.WebdriverHolder;
import org.semeniv.pages.AppCurrency;
import org.semeniv.pages.BasePage;
import org.semeniv.steps.ProductActionsSteps;
import org.semeniv.utils.PropertyReader;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;


public class BaseTest {
    protected ProductActionsSteps  productActionsSteps = new ProductActionsSteps();

    @AfterMethod
    public void clearCash(){
        WebdriverHolder.getInstance().getDriver().manage().deleteAllCookies();
        WebdriverHolder.getInstance().getDriver().navigate().refresh();
    }

    @BeforeMethod
    public void beforeClass() {
        WebdriverHolder.getInstance().getDriver().get(PropertyReader.getProperty("base.url"));
        new BasePage()
                .mainMenu()
                .selectLogin()
                .login(PropertyReader.getProperty("user.name"), PropertyReader.getProperty("user.password"), false);
        new BasePage().selectCurrency(AppCurrency.USD);
    }

    @AfterSuite
    public void afterSuite(){
        WebdriverHolder.getInstance().getDriver().quit();
    }
}

